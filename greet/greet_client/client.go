package main

import (
	"context"
	greet "dauka1/greet/greetpb"
	"fmt"
	"google.golang.org/grpc"
	"io"
	"log"
	"os"
)

func main() {
	fmt.Println("Hello I'm a client")

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := greet.NewGreetServiceClient(conn)
	//doGreetingEveryone(c)
	doManyTimesFromServer(c)
	os.Exit(1)
}

func doManyTimesFromServer(c greet.GreetServiceClient) {
	ctx := context.Background()
	req := &greet.PrimaryDecompositionRequest{RequestNum: int32(120)}

	stream, err := c.GreetManyTimes(ctx, req)
	if err != nil {
		log.Fatalf("error while calling GreetManyTimes RPC %v", err)
	}
	defer stream.CloseSend()

LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				// we've reached the end of the stream
				break LOOP
			}
			log.Fatalf("error while reciving from GreetManyTimes RPC %v", err)
		}
		log.Printf("response from Server RESULT NUM:%v \n", int(res.GetResponseNum()))
	}

}

